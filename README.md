# card-game

## Description

On souhaite développer un jeu de cartes.
Dans ce jeu, un joueur tire une main de 10 cartes de manière aléatoire.
Chaque carte possède une couleur ("Carreaux", par exemple) et une valeur ("10", par exemple).
On vous demande de:

    Construire un ordre aléatoire des couleurs. L'ordre des couleurs est, par exemple, l'un des suivants :

    --> Carreaux, Coeur, Pique, Trèfle

    Construire un ordre aléatoire des valeurs. L'ordre des valeurs est, par exemple, l'un des suivants :

    --> As, 5, 10, 8, 6, 7, 4, 2, 3, 9, Dame, Roi, Valet

    Construire une main de 10 cartes de manière aléatoire.
    Présenter la main "non triée" à l'écran puis la main triée selon n'importe quel ordre défini dans la 1ère et 2ème étape. C'est-à-dire que vous devez classer les cartes par couleur et valeur.

# Installation  and Requirements
The project is developed with Spring Boot and java 11
clone project
```
mvn clean insteall
mvn sprint-boot:run
```

## Design and requirements

- Configuration package : contains cors origin configuration
- controllers :contains hand controller responsible for generating/sorting the cards
- services: contains business logic 
- dao: contains models
- utils: utility functions for cards
- test: unit/integration testing 
##  Notes concerning the project
- Services have interfaces and interface implementation the choice was made to 
abstract the business logic.
- The hand size is 10 according to the description, however in the implementation is passed as an argument, to ensure extensibility.
- The order of the suits and ranks are generated randomly as the description, however a SortCriteria Modal is passed as an argument to ensure extensibility (e.g a user story where the user wants a custom order)
