package com.example.cardgame.controllers;

import com.example.cardgame.dao.HandDTO;
import com.example.cardgame.services.HandService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: Ilyes Hamoruni
 * @Date: 23/11/2021
 * Controller class the exposes endpoints for the player hand.
 */

@RestController
@RequestMapping(value = "/v1/hand")
public class HandController {

    private final HandService handService;

    public HandController(HandService handService){
        this.handService= handService;
    }

    @GetMapping(value = "/generate-random-hand/{numberOfCards}")
    public ResponseEntity<HandDTO> generateRandomHand(@PathVariable final int numberOfCards){
        return ResponseEntity.ok().body(this.handService.generateRandomHand(numberOfCards));
    }

    @PostMapping(value = "/sort-hand")
    public ResponseEntity<HandDTO> sortHand(@RequestBody HandDTO handDTO){
        return ResponseEntity.ok().body(this.handService.sortHand(handDTO));
    }

}
