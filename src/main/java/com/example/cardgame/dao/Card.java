package com.example.cardgame.dao;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Objects;

/**
 * @Author: Ilyes Hamrouni
 * @Date: 23/11/2021
 * model of Card Class, a card has a suite (diamond,spades etc) and a rank ( 2, queen, ace)
 */

@Getter
@Setter
@NoArgsConstructor
@ToString
public class Card {
   private Suite suite;
   private Rank rank;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Card)) return false;
        Card card = (Card) o;
        return Objects.equals(getSuite(), card.getSuite()) && Objects.equals(getRank(), card.getRank());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSuite(), getRank());
    }
}
