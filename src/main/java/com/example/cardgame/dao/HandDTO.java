package com.example.cardgame.dao;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @Author: ilyes Hamrouni
 * @Date: 23/11/2021
 * The hand dealt to the player. A hand consists of a set of cards and a sort criteria in which the cards appear.
 */

@NoArgsConstructor
@Getter
@Setter
@ToString
public class HandDTO {
    private  List<Card> hand;
    private SortCriteria sortCriteria;

}
