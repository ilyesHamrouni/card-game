package com.example.cardgame.dao;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @Author: Ilyes Hamrouni
 * @Date: 23/11/2021
 * Rank is the value of the card
 */
@RequiredArgsConstructor
@Getter
public enum Rank {
    ace("ace"),
    two("2"),
    three("3"),
    four("4"),
    five("5"),
    six("6"),
    seven("7"),
    eight("8"),
    nine("9"),
    ten("10"),
    jack("jack"),
    queen("queen"),
    king("king");
    private final String rank;
}
