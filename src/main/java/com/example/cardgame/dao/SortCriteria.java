package com.example.cardgame.dao;

import com.example.cardgame.dao.Rank;
import com.example.cardgame.dao.Suite;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class SortCriteria {
   private List<Suite> suiteSortCriteria;
   private List<Rank> rankSortCriteria;

}
