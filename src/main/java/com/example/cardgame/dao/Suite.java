package com.example.cardgame.dao;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @Author: Ilyes Hamrouni
 * @Date: 23/11/2021
 * Suite is the type of the card.
 */
@RequiredArgsConstructor
@Getter
public enum Suite {
    DIAMONDS("diamonds"),
    CLUBS("clubs"),
    HEARTS("hearts"),
    SPADES("spades");
    private  final String suite;
}
