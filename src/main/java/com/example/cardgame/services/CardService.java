package com.example.cardgame.services;

import com.example.cardgame.dao.Card;
import com.example.cardgame.dao.SortCriteria;
import java.util.List;

/**
 * @Author: ilyes Hamrouni
 * @Date: 23/11/2021
 * Interface of the card service, to abstract business logic
 */

public interface CardService {

    List<Card> generateRandomCards(int numberOfCards);
    List<Card> sortCards(List<Card> cards, SortCriteria sortCriteria);


}
