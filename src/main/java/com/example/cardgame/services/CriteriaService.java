package com.example.cardgame.services;

import com.example.cardgame.dao.SortCriteria;

/**
 * @Author: Ilyes Hamrouni
 * @Date: 23/11/2021
 * Criteria Interface
 */
public interface CriteriaService {
    SortCriteria generateRandomSortCriteria();
}
