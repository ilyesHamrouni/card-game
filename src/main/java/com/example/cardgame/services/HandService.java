package com.example.cardgame.services;

import com.example.cardgame.dao.HandDTO;

/**
 * @Author: ilyes hamrouni
 * @Date: 23/11/2021
 */
public interface HandService {

    HandDTO generateRandomHand(int numberOfCards);
    HandDTO sortHand(HandDTO handDTO);
}
