package com.example.cardgame.services.impl;

import com.example.cardgame.dao.Card;
import com.example.cardgame.dao.SortCriteria;
import com.example.cardgame.services.CardService;
import com.example.cardgame.utils.CardUtils;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @Author: Ilyes Hamrouni
 * @Date: 23/11/2021
 * Card Service responsible for card manipulations such generating a list of cards or sorting cards
 */

@Service
public class CardServiceImpl implements CardService {

    private final CardUtils cardUtils;

    public CardServiceImpl(CardUtils cardUtils){
        this.cardUtils = cardUtils;
    }

    /**
     * Generate a random Card
     * @return card
     */
    private Card generateRandomCard(){
        Card card = new Card();
        card.setSuite(this.cardUtils.getRandomSuite());
        card.setRank(this.cardUtils.getRandomRank());
        return card;
    }

    /**
     * Generate random cards
     * @param numberOfCards number of cards to be generated
     * @return cards
     */
    @Override
    public List<Card> generateRandomCards(int numberOfCards) {
        List<Card> cards = new ArrayList<>();
        //todo implement intstream instead of for loop. Currently a problem with my harddrive.
//        IntStream.of(numberOfCards).forEach(elem-> cards.add(this.generateRandomCard()));
        for(int index=0; index<numberOfCards;index++){
            cards.add(this.generateRandomCard());
            System.out.println(index);
        }
        return cards;
    }

    /**
     * sort cards
     * @param cards unsorted cards
     * @param sortCriteria sort criteria for suite and rank
     * @return sorted cards
     */
    @Override
    public List<Card> sortCards(List<Card> cards,SortCriteria sortCriteria) {
       return  cards.stream().sorted(comparator(sortCriteria)).collect(Collectors.toList());
    }

    /**
     * compares
     * @param sortCriteria sort criteria of order
     * @return comparator of card
     */
    private Comparator<Card> comparator(SortCriteria sortCriteria) {
        Comparator<Card> colorComparator = Comparator.comparingInt(card -> sortCriteria.getSuiteSortCriteria().indexOf(card.getSuite()));
        Comparator<Card> valuesComparator = Comparator.comparingInt(card -> sortCriteria.getRankSortCriteria().indexOf(card.getRank()));
        return colorComparator.thenComparing(valuesComparator);
    }

}
