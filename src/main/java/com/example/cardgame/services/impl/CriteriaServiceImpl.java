package com.example.cardgame.services.impl;

import com.example.cardgame.dao.SortCriteria;
import com.example.cardgame.services.CriteriaService;
import com.example.cardgame.utils.CardUtils;
import org.springframework.stereotype.Service;

/**
 * @Author: ilyes hamrouni
 * @Date: 23/11/2021
 * Criteria service used to generate random sort criteria
 */
@Service
public class CriteriaServiceImpl implements CriteriaService {
    private final CardUtils cardUtils;

    public CriteriaServiceImpl(CardUtils cardUtils){
        this.cardUtils =cardUtils;
    }

    /**
     * generates a random sort criteria
     * @return SortCriteria
     */
    @Override
    public SortCriteria generateRandomSortCriteria() {
            SortCriteria sortCriteria = new SortCriteria();
            sortCriteria.setSuiteSortCriteria(this.cardUtils.generateRandomSuites());
            sortCriteria.setRankSortCriteria(this.cardUtils.generateRandomRanks());
            return sortCriteria;

    }
}
