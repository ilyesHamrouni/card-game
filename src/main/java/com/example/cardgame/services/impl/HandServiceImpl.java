package com.example.cardgame.services.impl;

import com.example.cardgame.dao.HandDTO;
import com.example.cardgame.services.CardService;
import com.example.cardgame.services.CriteriaService;
import com.example.cardgame.services.HandService;
import org.springframework.stereotype.Service;

/**
 * @Author: ilyes hamrouni
 * @Date: 23/11/2021
 * Hand service responsible for generating and sorting a hand
 */
@Service
public class HandServiceImpl implements HandService {

    private final CardService cardService;
    private final CriteriaService criteriaService;

    public HandServiceImpl(CardService cardService, CriteriaService criteriaService){
        this.cardService = cardService;
        this.criteriaService = criteriaService;
    }

    /**
     * generates a random hand
     * @param numberOfCards total cards in the hand
     * @return hand
     */
    @Override
    public HandDTO generateRandomHand(int numberOfCards) {
        HandDTO handDTO = new HandDTO();
        handDTO.setHand(this.cardService.generateRandomCards(numberOfCards));
        handDTO.setSortCriteria(this.criteriaService.generateRandomSortCriteria());
        return handDTO;
    }

    /**
     * sorts the hand accoring the sort criteria
     * @param handDTO has the cards to sort and the sort criteria
     * @return sorted hand
     */
    @Override
    public HandDTO sortHand(HandDTO handDTO) {
        handDTO.setHand(this.cardService.sortCards(handDTO.getHand(),handDTO.getSortCriteria()));
        return handDTO;
    }
}
