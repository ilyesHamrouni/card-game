package com.example.cardgame.utils;

import com.example.cardgame.dao.Rank;
import com.example.cardgame.dao.Suite;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CardUtils {



    public Suite getRandomSuite(){
        int NUMBER_OF_TYPES = 4;
        return getAllSuites().get(this.generateRandomNumberInInterval(NUMBER_OF_TYPES -1));
    }

    public Rank getRandomRank(){
        int NUMBER_OF_CARD_VALUES = 14;
        return getAllRanks().get(this.generateRandomNumberInInterval(NUMBER_OF_CARD_VALUES -1));
    }

    public List<Suite> generateRandomSuites(){
        List<Suite> shuffledCardTypes= this.getAllSuites();
        Collections.shuffle(shuffledCardTypes);
        return shuffledCardTypes;
    }
    public List<Rank> generateRandomRanks(){
        List<Rank> shuffledCardValues= this.getAllRanks();
        Collections.shuffle(shuffledCardValues);
        return shuffledCardValues;
    }

    private int generateRandomNumberInInterval(int max){
        Random random = new Random();
        int number;
        number = random.nextInt(max);
        return number;
    }

    private List<Suite> getAllSuites(){
        return new ArrayList<>(List.of(Suite.CLUBS,Suite.DIAMONDS,Suite.SPADES,Suite.HEARTS));
    }

    private List<Rank> getAllRanks() {
        return new ArrayList<>(List.of(Rank.ace,Rank.two,Rank.three,Rank.four,Rank.five,Rank.six,Rank.seven,Rank.eight,Rank.nine,Rank.ten,Rank.jack,Rank.queen,Rank.king));
    }



}
