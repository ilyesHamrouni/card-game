package com.example.cardgame.controllers;

import com.example.cardgame.dao.Card;
import com.example.cardgame.dao.Rank;
import com.example.cardgame.dao.Suite;
import com.example.cardgame.dao.HandDTO;
import com.example.cardgame.services.HandService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.assertj.core.api.Assertions.assertThat;


import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(value = HandController.class)
class HandControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HandService handService;

    private HandDTO mockHand;

    @BeforeEach
    public  void initialize(){
        // build hand test data
        mockHand = new HandDTO();
        Card card1= new Card();
        card1.setSuite(Suite.CLUBS);
        card1.setRank(Rank.ace);
        Card card2 = new Card();
        card2.setSuite(Suite.DIAMONDS);
        card2.setRank(Rank.five);
        mockHand.setHand(new ArrayList<>(List.of(card1,card2)));

    }

    @Test
    public void generateRandomHandTest() throws Exception {

        String inputInJson = this.mapToJson(mockHand);

       final String URI = "/v1/hand/generate-random-hand/2";

        Mockito.when(handService.generateRandomHand(Mockito.anyInt())).thenReturn(mockHand);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        String outputInJson = response.getContentAsString();

        assertThat(outputInJson).isNotNull().isEqualTo(inputInJson);
        assertEquals(HttpStatus.OK.value(), response.getStatus());

    }

    @Test
    public void sortHandTest() throws Exception {
        String inputInJson = this.mapToJson(this.mockHand);

        final String URI = "/v1/hand/sort-hand";

        Mockito.when(handService.sortHand(Mockito.any(HandDTO.class))).thenReturn(mockHand);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
                .content(inputInJson).contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        String outputInJson = response.getContentAsString();

        assertThat(outputInJson).isNotNull();
        assertEquals(HttpStatus.OK.value(), response.getStatus());

    }

    /**
     * Maps an Object into a JSON String. Uses a Jackson ObjectMapper.
     */

    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }


}
