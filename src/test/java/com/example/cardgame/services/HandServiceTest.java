package com.example.cardgame.services;

import com.example.cardgame.dao.HandDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class HandServiceTest {

    @Autowired
    private HandService handService;
    @Test
    public void generateRandomHandTest(){
        HandDTO handDTO = this.handService.generateRandomHand(10);
        assertThat(handDTO).isNotNull();
        assertThat(handDTO.getHand()).isNotNull();
        assertThat(handDTO.getHand().size()).isEqualTo(10);

        assertThat(handDTO.getSortCriteria()).isNotNull();
        assertThat(handDTO.getSortCriteria().getSuiteSortCriteria()).isNotNull();
        assertThat(handDTO.getSortCriteria().getRankSortCriteria()).isNotNull();
    }

    @Test
    public void sortHandTest(){
        HandDTO handDTO = this.handService.generateRandomHand(10);

        HandDTO sortedHand = this.handService.sortHand(handDTO);

        assertThat(sortedHand.getHand()).isNotNull();

    }
}
