package com.example.cardgame.services.impl;

import com.example.cardgame.dao.Card;
import com.example.cardgame.dao.Rank;
import com.example.cardgame.dao.Suite;
import com.example.cardgame.dao.SortCriteria;
import com.example.cardgame.services.CardService;
import com.example.cardgame.utils.CardUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CardServiceImplTest {
    @Autowired
    private CardService cardService;
    @Autowired
    private CardUtils cardUtils;

    private List<Card> cards ;
    private SortCriteria  sortCriteria;
    @BeforeEach
    public void initialize(){
        //setup cards list
        cards = new ArrayList<>();
        Card card1= new Card();
        card1.setSuite(Suite.CLUBS);
        card1.setRank(Rank.ace);
        Card card2 = new Card();
        card2.setSuite(Suite.DIAMONDS);
        card2.setRank(Rank.five);
        Card card3= new Card();
        card3.setSuite(Suite.SPADES);
        card3.setRank(Rank.jack);
        Card card4= new Card();
        card4.setSuite(Suite.SPADES);
        card4.setRank(Rank.nine);
        cards=new ArrayList<>(List.of(card1,card2,card3,card4));
        //setup sort criteria ;

        sortCriteria = new SortCriteria();
        sortCriteria.setSuiteSortCriteria(List.of(Suite.SPADES,Suite.HEARTS,Suite.CLUBS,Suite.DIAMONDS));
        sortCriteria.setRankSortCriteria(List.of(Rank.nine,Rank.jack,Rank.ace,Rank.five));
    }

    @Test
    public void generateRandomCardsTest(){
        int numberOfCardsTest1=10;
        int numberOfCardsTest2=0;
        assertThat(this.cardService.generateRandomCards(numberOfCardsTest1)).isNotNull();
        assertThat(this.cardService.generateRandomCards(numberOfCardsTest1).size()).isEqualTo(10);

        assertThat(this.cardService.generateRandomCards(numberOfCardsTest2)).isNotNull();
        assertThat(this.cardService.generateRandomCards(numberOfCardsTest2).size()).isEqualTo(0);

    }

    @Test
    public void sortCardsTest(){
        List<Card> sortedCards= this.cardService.sortCards(this.cards,this.sortCriteria);
        assertThat(sortedCards).isNotNull();
        //test suite order
        assertThat(sortedCards.get(0).getSuite()).isEqualTo(Suite.SPADES);
        assertThat(sortedCards.get(3).getSuite()).isEqualTo(Suite.DIAMONDS);
        assertThat(sortedCards.get(2).getSuite()).isEqualTo(Suite.CLUBS);
        assertThat(sortedCards.get(1).getSuite()).isEqualTo(Suite.SPADES);
        //test rank order
        assertThat(sortedCards.get(0).getRank()).isEqualTo(Rank.nine);
        assertThat(sortedCards.get(3).getRank()).isEqualTo(Rank.five);

    }


}
