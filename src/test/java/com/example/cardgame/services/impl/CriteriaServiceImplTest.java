package com.example.cardgame.services.impl;

import com.example.cardgame.dao.SortCriteria;
import com.example.cardgame.services.CriteriaService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CriteriaServiceImplTest {

    @Autowired
    private CriteriaService criteriaService;

    @Test
    public void generateRandomSortCriteriaTest(){
        SortCriteria sortCriteria = this.criteriaService.generateRandomSortCriteria();

        assertThat(sortCriteria).isNotNull();
        assertThat(sortCriteria.getRankSortCriteria()).isNotNull();
        assertThat(sortCriteria.getSuiteSortCriteria()).isNotNull();
    }
}
